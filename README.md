# A collection of weird/slightly out-of-spec OpenPGP keys

Despite the name of this repository, we are not collecting weird keys
that we come across, or randomly generate weird keys.  Every one of
these keys is carefully crafted to check how much various OpenPGP
implementations need to be changed in order to support our various
goals for OpenPGP:

  * multi-device support
  * forward secrecy
  * privacy-enhanced key servers

## two.pgp

This key has two encryption subkeys, both "may be used to encrypt
communications" (key flag 0x04), and "may be used to encrypt storage"
(key flag 0x08).

This key is a valid OpenPGP key.

The purpose of this test is to see whether or not implementations
encrypt for both keys.

Please encrypt a message with this key, and inspect the encrypted
message (e.g. with `sq dump`) to see how many PKESK packets are
included.

## archive.pgp

This key has two encryption subkeys, one with that "may be used to
encrypt communications" (key flag 0x04), and one that "may be used to
encrypt storage" (key flag 0x08).

This key is a valid OpenPGP key.

The purpose of this test is to see whether or not implementations
discriminate between the two.

Please encrypt a message with this key, and inspect the encrypted
message (e.g. with `sq dump`) to see which key was used for
encryption.  Please check whether or not your implementation allows
you to select the purpose of the encryption, i.e. encrypting data at
rest (e.g. for backups) or for transit (e.g. mails).

## approx.pgp

This key has 52 encryption subkeys, one for each week.  This key tries
to approximate forward secrecy.

This key is a valid OpenPGP key.

The purpose of this test is to see whether or not implementations
pick the right key.

Please encrypt a message with this key, and inspect the encrypted
message (e.g. with `sq dump`) to see which key was used for
encryption.  Check whether or not the implementation picked the key
that is valid for the current week, i.e. the creation date is in the
past and the expiration date is in the future.

## cert-subkeys.pgp

This key has two certification-capable subkeys, each certifying one
encryption capable subkey.  This is our proposed model for
multi-device support without the need for synchronization.

This key may or may not be a valid OpenPGP key.

The purpose of this test is to see whether or not implementations
accept subkeys bound by certification-capable subkeys.

Please try to encrypt a message with this key.  Does it work at all?

## null-uid.pgp

This key has a UID of length zero.

This key is a valid OpenPGP key.

The purpose of this test is to see whether or not implementations
handle this key correcly.

Please encrypt a message with this key.  Is the key correctly listed?
Is something weird?

## no-bound-uid.pgp

This key has a UID that has no binding signature.

This key is a valid OpenPGP key (this may be an oversight in the
spec).

The purpose of this test is to see whether or not implementations
handle this key correcly.

Please encrypt a message with this key.  Is the key correctly listed?
Is something weird?

## no-uid.pgp

This key has no UID.

This key is NOT a valid OpenPGP key.

The purpose of this test is to see whether or not implementations
handle this key correcly.

Please encrypt a message with this key.  Is the key correctly listed?
Is something weird?

## direct-key.pgp

This key has no UID, therefore there is no uid binding signature on
which the primary key properties such as key flags, and expiration
time can be specified.  This key carries a direct-key signature with
the subpackets normally found on a uid binding signature (key flags,
and key expiration time).

This key is NOT a valid OpenPGP key.

The purpose of this test is to see whether or not implementations
handle this key correcly, and whether implementations honor the key
properties contained in the direct-key signature.

Please encrypt a message with this key.  Is the key correctly listed?
Is something weird?  Does the implementation honor the key flags
(certificate, sign) for the primary key?  Does the implementation
honor the key expiration time for the primary key?


# Results

|              | Sequoia (2018-07-06) | GnuPG 2.2.8    | OpenKeychain 5.1.4 | openpgp.js 3.0.12 | rnp (2018-07-05) | DKGPG (2018-09-01) |
| ---          | ---                  | ---            | ---                | ---               | ---              | ---                |
| two          | ✓                    | ✗ <sup>1</sup> | ✓                  | ✗ <sup>1</sup>    | ✗ <sup>1</sup>   | ✓                  |
| archive      | ✓                    | ✗ <sup>2</sup> | ✗ <sup>2</sup>     | ✗ <sup>2</sup>    | ✗ <sup>2</sup>   | ✗ <sup>2</sup>     |
| approx       | ✓                    | ✓              | ✗ <sup>3</sup>     | ✓                 | ✗ <sup>5</sup>   | ✓                  |
| cert-subkeys | ✗                    | ✗              | ✗                  | ✗                 | ✓ <sup>6</sup>   | ✗                  |
| null-uid     | ✓                    | ✓              | ✓ <sup>4</sup>     | ✓                 | ✗                | ✓                  |
| no-bound-uid | ✓                    | ✗              | ✗                  | ✗                 | ✗ <sup>7</sup>   | ✓ <sup>8</sup>     |
| no-uid       | ✓                    | ✗              | ✗                  | ✗                 | ✗ <sup>7</sup>   | ✗                  |
| direct-key   | ✗                    | ✗              | ✗                  | ?                 | ✓                | ✗                  |

Remarks:
1. Encrypts to the first suitable key only
2. Does not discriminate.
3. Strips subkeys with binding signatures predating their keys.  Strips subkeys with a creation date in the future on import.  [#2374](https://github.com/open-keychain/open-keychain/issues/2374)
4. Slight graphical glitch: Immediately after importing, showed the email address of the preceding key in the main window's key list.  [#2373](https://github.com/open-keychain/open-keychain/issues/2373)
5. Does not honor the key's creation time, i.e. uses a key before it is alive.
6. Actually encrypts to the first encryption subsubkey.  Might be merely a bug in the implementation though.
7. Assumes the primary key can encrypt, sign, certify, and authenticate, and therefore encrypts to it.
8. Only with option "-w" (allow weak keys)